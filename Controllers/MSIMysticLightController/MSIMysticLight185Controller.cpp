/*---------------------------------------------*\
|  MSIMysticLight185Controller.cpp              |
|                                               |
|  Driver for MSI Mystic Light (185-byte)       |
|  USB lighting controller                      |
|                                               |
|  T-bond 3/4/2020                              |
|  Adam Honse 3/6/2021                          |
|                                               |
| The direct mode part has been implemented     |
| based on the mystic-why project provided by   |
| Aleksandr Garashchenko                        |
| (https://github.com/garashchenko/mystic-why)  |
\*---------------------------------------------*/

#include "MSIMysticLight185Controller.h"
#include <algorithm>
#include <array>
#include <bitset>
#include <map>


struct Config
{
    Config(int _numofOnboardLeds, std::size_t _numofZones, const MSI_ZONE* _zones) :
        numofOnboardLeds(_numofOnboardLeds),
        numofZones(_numofZones),
        supportedZones(_zones) {};
    int             numofOnboardLeds;
    std::size_t     numofZones;
    const MSI_ZONE* supportedZones;
};

static const MSI_ZONE allZones[] =
{
    MSI_ZONE_J_RGB_1,
    MSI_ZONE_J_RGB_2,
    MSI_ZONE_J_RAINBOW_1,
    MSI_ZONE_J_RAINBOW_2,
    MSI_ZONE_J_CORSAIR,
    MSI_ZONE_J_PIPE_1,
    MSI_ZONE_J_PIPE_2,
    MSI_ZONE_ON_BOARD_LED_0,
    MSI_ZONE_ON_BOARD_LED_1,
    MSI_ZONE_ON_BOARD_LED_2,
    MSI_ZONE_ON_BOARD_LED_3,
    MSI_ZONE_ON_BOARD_LED_4,
    MSI_ZONE_ON_BOARD_LED_5,
    MSI_ZONE_ON_BOARD_LED_6,
    MSI_ZONE_ON_BOARD_LED_7,
    MSI_ZONE_ON_BOARD_LED_8,
    MSI_ZONE_ON_BOARD_LED_9
};

static const MSI_ZONE zonesSet0[] =
{
    MSI_ZONE_J_RGB_1,
    MSI_ZONE_J_RGB_2,
    MSI_ZONE_J_RAINBOW_1,
    MSI_ZONE_J_RAINBOW_2,
    MSI_ZONE_ON_BOARD_LED_0
};


// Definition of the board sepcific configurations (number of onboard LEDs and supported zones).
//
// Only tested boards are listed here (refer to MSIMysticLightControllerDetect.cpp). If more boards
// are tested the list must be extended here. Otherwise the default settings will be used (7 onboard LEDs, all zones supported).
// Boards with yet unknown supported zones are configured to support all zones.
//

static const std::map<unsigned short, Config> boardConfigs =
{
    {0x7B10, Config(23, sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MEG Z390 GODLIKE
    {0x7C34, Config(22, sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MEG X570 GODLIKE
    {0x7C56, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG B550 GAMING PLUS
    {0x7C70, Config(27, sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MEG Z490 GODLIKE
    {0x7C71, Config(13, sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MEG Z490 ACE
    {0x7C73, Config(11, sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG Z490 GAMING CARBON
    {0x7C75, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG Z490 GAMING PLUS
    {0x7C76, Config(8,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG Z490M GAMING EDGE
    {0x7C77, Config(6,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MEG Z490 ACE
    {0x7C79, Config(8,  sizeof(zonesSet0) / sizeof(MSI_ZONE), zonesSet0)},  // MPG Z490 GAMING EDGE WIFI
    {0x7C80, Config(8,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MAG Z490 TOMAHAWK
    {0x7C81, Config(8,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MAG B460 TOMAHAWK
    {0x7C82, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MAG B460M MORTAR
    {0x7C83, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MAG B460M BAZOOKA
    {0x7C86, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG B460I GAMING EDGE
    {0x7C85, Config(8,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // B460-A PRO
    {0x7C88, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // B460M-PRO
    {0x7C89, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // H410M-PRO
    {0x7C90, Config(11, sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG B550 GAMING CARBON WIFI
    {0x7C91, Config(8,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG B550 GAMING EDGE MAX WIFI
    {0x7C92, Config(6,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG B550I GAMING EDGE WIFI
    {0x7C94, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MAG B550M MORTAR
    {0x7C95, Config(8,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MAG B550M BAZOOKA
    {0x7C98, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // Z490-S01
    {0x7C99, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // Z490M-S01
    {0x7D06, Config(9,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)},   // MPG Z590 GAMING FORCE
    {0x7D09, Config(7,  sizeof(allZones) / sizeof(MSI_ZONE),  allZones)}    // Z590-A PRO
};


static FeaturePacket_185 enablePerLedMsg;

static Color* per_led_on_board_leds;
static Color* per_led_j_rainbow1;
static Color* per_led_j_rainbow2;
static Color* per_led_j_corsair;


MSIMysticLight185Controller::MSIMysticLight185Controller(hid_device* handle, const char *path, unsigned short pid)
{
    dev = handle;

    if (dev)
    {
        location = path;

        ReadName();
        ReadSerial();
        ReadFwVersion();
        ReadSettings();
    }

    /*-----------------------------------------*\
    | Initialize save flag                      |
    \*-----------------------------------------*/
    data.save_data = 0;

    /*-----------------------------------------*\
    | Initialize Per LED data                   |
    \*-----------------------------------------*/
    auto boardConfig = boardConfigs.find(pid);

    if (boardConfig != boardConfigs.end())
    {
        onBoardLedCount = boardConfig->second.numofOnboardLeds;
        numofZones = boardConfig->second.numofZones;
        supportedZones = boardConfig->second.supportedZones;
    }
    else
    {
        onBoardLedCount = 7;
        numofZones = sizeof(allZones) / sizeof(MSI_ZONE);
        supportedZones = allZones;
    }

    std::memset(perLedData.leds, 0, sizeof(Color) * NUMOF_PER_LED_MODE_LEDS);
    per_led_on_board_leds = perLedData.leds;
    per_led_j_rainbow1 = per_led_on_board_leds + onBoardLedCount;
    per_led_j_rainbow2 = per_led_j_rainbow1 + PER_LED_MODE_JRAINBOW_LED_COUNT;
    per_led_j_corsair = per_led_j_rainbow2 + PER_LED_MODE_JRAINBOW_LED_COUNT;

    // Set up per LED switching message.
    // Static initialization made problems with some compilers. Therefore this is done programmatically here.
    enablePerLedMsg.j_rgb_1.color.R = 0xFF;
    enablePerLedMsg.j_rgb_1.speedAndBrightnessFlags = 0x08;
    enablePerLedMsg.j_rgb_1.color2.R = 0xFF;
    enablePerLedMsg.j_rgb_1.colorFlags = 0x80;
    enablePerLedMsg.j_pipe_1.color.R = 0xFF;
    enablePerLedMsg.j_pipe_1.speedAndBrightnessFlags = 0x2A;
    enablePerLedMsg.j_pipe_1.color2.R = 0xFF;
    enablePerLedMsg.j_pipe_1.colorFlags = 0x80;
    enablePerLedMsg.j_pipe_2.color.R = 0xFF;
    enablePerLedMsg.j_pipe_2.speedAndBrightnessFlags = 0x2A;
    enablePerLedMsg.j_pipe_2.color2.R = 0xFF;
    enablePerLedMsg.j_pipe_2.colorFlags = 0x80;
    enablePerLedMsg.j_rainbow_1.color.R = 0xFF;
    enablePerLedMsg.j_rainbow_1.speedAndBrightnessFlags = 0x08;
    enablePerLedMsg.j_rainbow_1.color2.R = 0xFF;
    enablePerLedMsg.j_rainbow_1.colorFlags = 0x80;
    enablePerLedMsg.j_rainbow_2.color.R = 0xFF;
    enablePerLedMsg.j_rainbow_2.speedAndBrightnessFlags = 0x08;
    enablePerLedMsg.j_rainbow_2.color2.R = 0xFF;
    enablePerLedMsg.j_rainbow_2.colorFlags = 0x80;
    enablePerLedMsg.j_corsair.effect = MSI_MODE_DISABLE;
    enablePerLedMsg.j_corsair.corsair_quantity = 0x00;
    enablePerLedMsg.j_corsair.padding[0] = 0x00;
    enablePerLedMsg.j_corsair.padding[1] = 0x00;
    enablePerLedMsg.j_corsair.padding[2] = 0x82;
    enablePerLedMsg.j_corsair.padding[3] = 0x00;
    enablePerLedMsg.j_corsair.is_individual = 0x78;
    enablePerLedMsg.j_corsair_outerll120.color.R = 0xFF;
    enablePerLedMsg.j_corsair_outerll120.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.j_corsair_outerll120.colorFlags = 0x80;
    enablePerLedMsg.on_board_led.effect = 0x25;
    enablePerLedMsg.on_board_led.color.R = 0xFF;
    enablePerLedMsg.on_board_led.speedAndBrightnessFlags = 0xA9;
    enablePerLedMsg.on_board_led.color2.R = 0xFF;
    enablePerLedMsg.on_board_led.colorFlags = 0x87;
    enablePerLedMsg.on_board_led_1.color.R = 0xFF;
    enablePerLedMsg.on_board_led_1.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_1.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_1.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_2.color.R = 0xFF;
    enablePerLedMsg.on_board_led_2.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_2.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_2.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_3.color.R = 0xFF;
    enablePerLedMsg.on_board_led_3.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_3.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_3.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_4.color.R = 0xFF;
    enablePerLedMsg.on_board_led_4.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_4.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_4.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_5.color.R = 0xFF;
    enablePerLedMsg.on_board_led_5.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_5.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_5.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_6.color.R = 0xFF;
    enablePerLedMsg.on_board_led_6.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_6.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_6.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_7.color.R = 0xFF;
    enablePerLedMsg.on_board_led_7.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_7.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_7.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_8.color.R = 0xFF;
    enablePerLedMsg.on_board_led_8.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_8.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_8.colorFlags = 0x80;
    enablePerLedMsg.on_board_led_9.color.R = 0xFF;
    enablePerLedMsg.on_board_led_9.speedAndBrightnessFlags = 0x28;
    enablePerLedMsg.on_board_led_9.color2.G = 0xFF;
    enablePerLedMsg.on_board_led_9.colorFlags = 0x80;
    enablePerLedMsg.j_rgb_2.color.R = 0xFF;
    enablePerLedMsg.j_rgb_2.speedAndBrightnessFlags = 0x2A;
    enablePerLedMsg.j_rgb_2.color2.R = 0xFF;
    enablePerLedMsg.j_rgb_2.colorFlags = 0x80;

    directMode = false;
}

MSIMysticLight185Controller::~MSIMysticLight185Controller()
{
    hid_close(dev);
}

unsigned int MSIMysticLight185Controller::GetZoneMinLedCount(MSI_ZONE /*zone*/)
{
    return 1;
}

unsigned int MSIMysticLight185Controller::GetZoneMaxLedCount(MSI_ZONE zone)
{
    switch (zone)
    {
        case MSI_ZONE_J_RAINBOW_1:
        case MSI_ZONE_J_RAINBOW_2:
            return PER_LED_MODE_JRAINBOW_LED_COUNT;
        case MSI_ZONE_J_CORSAIR:
            //return PER_LED_MODE_CORSAIR_LED_COUNT;
            return 1;   // not tested, therefore return 1
        case MSI_ZONE_ON_BOARD_LED_0:
            return onBoardLedCount;
    default:
        return 1;
    }
}

unsigned int MSIMysticLight185Controller::GetZoneLedCount(MSI_ZONE zone)
{
    RainbowZoneData *requestedZone = GetRainbowZoneData(zone);

    if (!requestedZone)
    {
        return GetZoneMaxLedCount(zone);
    }

    return requestedZone->cycle_or_led_num;
}

void MSIMysticLight185Controller::SetZoneLedCount(MSI_ZONE zone, unsigned int led_count)
{
    RainbowZoneData *requestedZone = GetRainbowZoneData(zone);

    if (!requestedZone)
    {
        return;
    }

    led_count = std::min(GetZoneMaxLedCount(zone), std::max(GetZoneMinLedCount(zone), led_count));
    requestedZone->cycle_or_led_num = led_count;
}

void MSIMysticLight185Controller::SetMode(MSI_ZONE zone,
                                          MSI_MODE mode,
                                          MSI_SPEED speed,
                                          MSI_BRIGHTNESS brightness,
                                          bool rainbow_color)
{
    ZoneData* zoneData = GetZoneData(zone);

    if (!zoneData)
    {
        return;
    }

    zoneData->effect                    = mode;
    zoneData->speedAndBrightnessFlags   = ( brightness << 2 ) | ( speed & 0x03 );
    zoneData->colorFlags                = BitSet(zoneData->colorFlags, !rainbow_color, 7u);
    zoneData->padding                   = 0x00;
}

std::string MSIMysticLight185Controller::GetDeviceName()
{
    return name;
}

std::string MSIMysticLight185Controller::GetFWVersion()
{
    return std::string("AP/LD ").append(version_APROM).append(" / ").append(version_LDROM);
}

std::string MSIMysticLight185Controller::GetDeviceLocation()
{
    return("HID: " + location);
}

std::string MSIMysticLight185Controller::GetSerial()
{
    return chip_id;
}

bool MSIMysticLight185Controller::ReadSettings()
{
    /*-----------------------------------------------------*\
    | Read packet from hardware, return true if successful  |
    \*-----------------------------------------------------*/
    return (hid_get_feature_report(dev, (unsigned char *)&data, sizeof(data)) == sizeof data);
}

bool MSIMysticLight185Controller::Update(bool save)
{
    /*-----------------------------------------------------*\
    | Send packet to hardware, return true if successful    |
    \*-----------------------------------------------------*/
    if (directMode)
    {
        return (hid_send_feature_report(dev, (unsigned char *)&perLedData, sizeof(perLedData)) == sizeof perLedData);
    }
    else
    {
        data.save_data = save;
        return (hid_send_feature_report(dev, (unsigned char *)&data, sizeof(data)) == sizeof data);
    }
}

void MSIMysticLight185Controller::SetZoneColor(MSI_ZONE zone,
                                               unsigned char red1,
                                               unsigned char grn1,
                                               unsigned char blu1,
                                               unsigned char red2,
                                               unsigned char grn2,
                                               unsigned char blu2)
{
    ZoneData* zoneData = GetZoneData(zone);

    if (!zoneData)
    {
        return;
    }

    zoneData->color.R = red1;
    zoneData->color.G = grn1;
    zoneData->color.B = blu1;

    zoneData->color2.R = red2;
    zoneData->color2.G = grn2;
    zoneData->color2.B = blu2;
}

void MSIMysticLight185Controller::SetLedColor(MSI_ZONE zone,
                                              int index,
                                              unsigned char red,
                                              unsigned char grn,
                                              unsigned char blu)
{
    Color* zoneData = GetPerLedZoneData(zone);

    if (zoneData != nullptr)
    {
        int maxSize = 0;

        switch (zone)
        {
            case MSI_ZONE_J_RAINBOW_1:
            case MSI_ZONE_J_RAINBOW_2:
                maxSize = PER_LED_MODE_JRAINBOW_LED_COUNT;
                break;

            case MSI_ZONE_ON_BOARD_LED_0:
                maxSize = onBoardLedCount;
                break;

            case MSI_ZONE_J_CORSAIR:
                maxSize = PER_LED_MODE_CORSAIR_LED_COUNT;
                break;

            default:
                break;
        }

        if (index < maxSize)
        {
            zoneData[index].R = red;
            zoneData[index].G = grn;
            zoneData[index].B = blu;
        }
    }
}


std::pair<Color, Color> MSIMysticLight185Controller::GetZoneColor(MSI_ZONE zone)
{
    ZoneData *zoneData = GetZoneData(zone);

    if (!zoneData)
    {
        return std::make_pair(Color{}, Color{});
    }

    return std::make_pair(Color{zoneData->color.R, zoneData->color.G, zoneData->color.B},
                          Color{zoneData->color2.R, zoneData->color2.G, zoneData->color2.B});
}

ZoneData *MSIMysticLight185Controller::GetZoneData(MSI_ZONE zone)
{
    switch (zone)
    {
        case MSI_ZONE_J_RGB_1:
            return &data.j_rgb_1;
        case MSI_ZONE_J_RGB_2:
            return &data.j_rgb_2;
        case MSI_ZONE_J_RAINBOW_1:
            return &data.j_rainbow_1;
        case MSI_ZONE_J_RAINBOW_2:
            return &data.j_rainbow_2;
        case MSI_ZONE_J_PIPE_1:
            return &data.j_pipe_1;
        case MSI_ZONE_J_PIPE_2:
            return &data.j_pipe_2;
        case MSI_ZONE_ON_BOARD_LED_0:
            return &data.on_board_led;
        case MSI_ZONE_ON_BOARD_LED_1:
            return &data.on_board_led_1;
        case MSI_ZONE_ON_BOARD_LED_2:
            return &data.on_board_led_2;
        case MSI_ZONE_ON_BOARD_LED_3:
            return &data.on_board_led_3;
        case MSI_ZONE_ON_BOARD_LED_4:
            return &data.on_board_led_4;
        case MSI_ZONE_ON_BOARD_LED_5:
            return &data.on_board_led_5;
        case MSI_ZONE_ON_BOARD_LED_6:
            return &data.on_board_led_6;
        case MSI_ZONE_ON_BOARD_LED_7:
            return &data.on_board_led_7;
        case MSI_ZONE_ON_BOARD_LED_8:
            return &data.on_board_led_8;
        case MSI_ZONE_ON_BOARD_LED_9:
            return &data.on_board_led_9;
        case MSI_ZONE_J_CORSAIR_OUTERLL120:
            return &data.j_corsair_outerll120;
        default:
        case MSI_ZONE_J_CORSAIR:
            break;
    }

    return nullptr;
}

Color *MSIMysticLight185Controller::GetPerLedZoneData(MSI_ZONE  zone)
{
    switch (zone)
    {
        case MSI_ZONE_J_RAINBOW_1:
            return per_led_j_rainbow1;
        case MSI_ZONE_J_RAINBOW_2:
            return per_led_j_rainbow2;
        case MSI_ZONE_ON_BOARD_LED_0:
            return per_led_on_board_leds;
        case MSI_ZONE_J_CORSAIR:
            return per_led_j_corsair;
        default:
            break;
    }

    return nullptr;
}

RainbowZoneData *MSIMysticLight185Controller::GetRainbowZoneData(MSI_ZONE zone)
{
    switch (zone)
    {
        case MSI_ZONE_J_RAINBOW_1:
            return &data.j_rainbow_1;
        case MSI_ZONE_J_RAINBOW_2:
            return &data.j_rainbow_2;
        case MSI_ZONE_J_CORSAIR:
        default:
            return nullptr;
    }
}

bool MSIMysticLight185Controller::ReadFwVersion()
{
    unsigned char request[64];
    unsigned char response[64];
    int ret_val = 64;

    /*-----------------------------------------------------*\
    | First read the APROM                                  |
    | Checksum also available at report ID 180, with MSB    |
    | stored at index 0x08 and LSB at 0x09                  |
    \*-----------------------------------------------------*/

    /*-----------------------------------------------------*\
    | Zero out buffers                                      |
    \*-----------------------------------------------------*/
    memset(request, 0x00, sizeof(request));
    memset(response, 0x00, sizeof(response));

    /*-----------------------------------------------------*\
    | Set up APROM Firmware Version Request packet          |
    \*-----------------------------------------------------*/
    request[0x00]   = 0x01;
    request[0x01]   = 0xB0;

    /*-----------------------------------------------------*\
    | Fill request from 0x02 to 0x61 with 0xCC              |
    \*-----------------------------------------------------*/
    memset(&request[0x02], 0xCC, sizeof(request) - 2);

    /*-----------------------------------------------------*\
    | Send request and receive response packets             |
    \*-----------------------------------------------------*/
    ret_val &= hid_write(dev, request, 64);
    ret_val &= hid_read(dev, response, 64);

    /*-----------------------------------------------------*\
    | Extract high and low values from response             |
    \*-----------------------------------------------------*/
    unsigned char highValue = response[2] >> 4;
    unsigned char lowValue  = response[2] & 0x0F;

    /*-----------------------------------------------------*\
    | Build firmware string <high>.<low>                    |
    \*-----------------------------------------------------*/
    version_APROM = std::to_string(static_cast<int>(highValue)).append(".").append(std::to_string(static_cast<int>(lowValue)));

    /*-----------------------------------------------------*\
    | First read the LDROM                                  |
    | Checksum also available at report ID 184, with MSB    |
    | stored at index 0x08 and LSB at 0x09                  |
    \*-----------------------------------------------------*/

    /*-----------------------------------------------------*\
    | Set up LDROM Firmware Version Request packet          |
    \*-----------------------------------------------------*/
    request[0x00]   = 0x01;
    request[0x01]   = 0xB6;

    /*-----------------------------------------------------*\
    | Send request and receive response packets             |
    \*-----------------------------------------------------*/
    ret_val &= hid_write(dev, request, 64);
    ret_val &= hid_read(dev, response, 64);

    /*-----------------------------------------------------*\
    | Extract high and low values from response             |
    \*-----------------------------------------------------*/
    highValue = response[2] >> 4;
    lowValue  = response[2] & 0x0F;

    /*-----------------------------------------------------*\
    | Build firmware string <high>.<low>                    |
    \*-----------------------------------------------------*/
    version_LDROM = std::to_string(static_cast<int>(highValue)).append(".").append(std::to_string(static_cast<int>(lowValue)));

    /*-----------------------------------------------------*\
    | If return value is zero it means an HID transfer      |
    | failed                                                |
    \*-----------------------------------------------------*/
    return(ret_val > 0);
}

void MSIMysticLight185Controller::ReadSerial()
{
    wchar_t serial[256];

    /*-----------------------------------------------------*\
    | Get the serial number string from HID                 |
    \*-----------------------------------------------------*/
    hid_get_serial_number_string(dev, serial, 256);

    /*-----------------------------------------------------*\
    | Convert wchar_t into std::wstring into std::string    |
    \*-----------------------------------------------------*/
    std::wstring wserial = std::wstring(serial);
    chip_id = std::string(wserial.begin(), wserial.end());
}

void MSIMysticLight185Controller::ReadName()
{
    wchar_t tname[256];

    /*-----------------------------------------------------*\
    | Get the manufacturer string from HID                  |
    \*-----------------------------------------------------*/
    hid_get_manufacturer_string(dev, tname, 256);

    /*-----------------------------------------------------*\
    | Convert wchar_t into std::wstring into std::string    |
    \*-----------------------------------------------------*/
    std::wstring wname = std::wstring(tname);
    name = std::string(wname.begin(), wname.end());

    /*-----------------------------------------------------*\
    | Get the product string from HID                       |
    \*-----------------------------------------------------*/
    hid_get_product_string(dev, tname, 256);

    /*-----------------------------------------------------*\
    | Append the product string to the manufacturer string  |
    \*-----------------------------------------------------*/
    wname = std::wstring(tname);
    name.append(" ").append(std::string(wname.begin(), wname.end()));
}

void MSIMysticLight185Controller::GetMode(MSI_ZONE zone,
                                          MSI_MODE &mode,
                                          MSI_SPEED &speed,
                                          MSI_BRIGHTNESS &brightness,
                                          bool &rainbow_color)
{
    /*-----------------------------------------------------*\
    | Get data for given zone                               |
    \*-----------------------------------------------------*/
    ZoneData *zoneData = GetZoneData(zone);

    /*-----------------------------------------------------*\
    | Return if zone is invalid                             |
    \*-----------------------------------------------------*/
    if (!zoneData)
    {
        return;
    }

    /*-----------------------------------------------------*\
    | Update pointers with data                             |
    \*-----------------------------------------------------*/
    mode            = (MSI_MODE)(zoneData->effect);
    speed           = (MSI_SPEED)(zoneData->speedAndBrightnessFlags & 0x03);
    brightness      = (MSI_BRIGHTNESS)((zoneData->speedAndBrightnessFlags >> 2) & 0x1F);
    rainbow_color   = (zoneData->colorFlags & 0x80) >> 7;
}

unsigned char MSIMysticLight185Controller::BitSet(unsigned char value, bool bit, unsigned int position)
{
    return static_cast<unsigned char>(std::bitset<8>(value).set(position, bit).to_ulong());
}

void MSIMysticLight185Controller::SetCycleCount(MSI_ZONE zone, unsigned char cycle_num)
{
    RainbowZoneData *requestedZone = GetRainbowZoneData(zone);

    if (!requestedZone)
    {
        return;
    }

    requestedZone->cycle_or_led_num = cycle_num;
}

unsigned char MSIMysticLight185Controller::GetCycleCount(MSI_ZONE zone)
{
    RainbowZoneData *requestedZone = GetRainbowZoneData(zone);

    if (!requestedZone)
    {
        return 0;
    }

    return requestedZone->cycle_or_led_num;
}


void MSIMysticLight185Controller::SetDirectMode(bool mode)
{
    directMode = mode;

    if (directMode)
    {
        hid_send_feature_report(dev, (unsigned char *)&enablePerLedMsg, sizeof(enablePerLedMsg));
    }
}
