/*-----------------------------------------*\
|  RGBController_MSIMysticLight185.cpp      |
|                                           |
|  Generic RGB Interface for OpenRGB        |
|  MSI Mystic Light (185-byte) USB Driver   |
|                                           |
|  T-bond 3/4/2020                          |
|  Adam Honse 3/6/2021                      |
\*-----------------------------------------*/

#include "RGBController_MSIMysticLight185.h"

#include <iostream>

struct ZoneDescription
{
    std::string name;
    int         minLeds;
    int         maxLeds;
    MSI_ZONE    zoneType;
};

#define NUMOF_ZONES     (sizeof(led_zones) / sizeof(ZoneDescription))

static const ZoneDescription led_zones[] =
{
    ZoneDescription{"JRGB1",        1, 1,                               MSI_ZONE_J_RGB_1 },
    ZoneDescription{"JRGB2",        1, 1,                               MSI_ZONE_J_RGB_2 },
    ZoneDescription{"JRAINBOW1",    1, PER_LED_MODE_JRAINBOW_LED_COUNT, MSI_ZONE_J_RAINBOW_1 },
    ZoneDescription{"JRAINBOW2",    1, PER_LED_MODE_JRAINBOW_LED_COUNT, MSI_ZONE_J_RAINBOW_2 },
    ZoneDescription{"JCORSAIR",     1, 1,                               MSI_ZONE_J_CORSAIR },
    ZoneDescription{"PIPE1",        1, 1,                               MSI_ZONE_J_PIPE_1 },
    ZoneDescription{"PIPE2",        1, 1,                               MSI_ZONE_J_PIPE_2 },
    ZoneDescription{"ONBOARD0",     1, 1,                               MSI_ZONE_ON_BOARD_LED_0 },
    ZoneDescription{"ONBOARD1",     1, 1,                               MSI_ZONE_ON_BOARD_LED_1 },
    ZoneDescription{"ONBOARD2",     1, 1,                               MSI_ZONE_ON_BOARD_LED_2 },
    ZoneDescription{"ONBOARD3",     1, 1,                               MSI_ZONE_ON_BOARD_LED_3 },
    ZoneDescription{"ONBOARD4",     1, 1,                               MSI_ZONE_ON_BOARD_LED_4 },
    ZoneDescription{"ONBOARD5",     1, 1,                               MSI_ZONE_ON_BOARD_LED_5 },
    ZoneDescription{"ONBOARD6",     1, 1,                               MSI_ZONE_ON_BOARD_LED_6 },
    ZoneDescription{"ONBOARD7",     1, 1,                               MSI_ZONE_ON_BOARD_LED_7 },
    ZoneDescription{"ONBOARD8",     1, 1,                               MSI_ZONE_ON_BOARD_LED_8 },
    ZoneDescription{"ONBOARD9",     1, 1,                               MSI_ZONE_ON_BOARD_LED_9 },
};


std::vector<const ZoneDescription*> zoneDescription;

// Returns the index of the ZoneDescription in led_zones which has zoneType equal to the given zone_type.
// Returns -1 if no such ZoneDescription exists.
int IndexOfZoneForType(MSI_ZONE zone_type) {
    for (int i = 0; i < zoneDescription.size(); i++) {
        if (zoneDescription[i]->zoneType == zone_type) {
            return i;
        }
    }
    return -1;
}

RGBController_MSIMysticLight185::RGBController_MSIMysticLight185(MSIMysticLight185Controller* controller_ptr)
{
    controller = controller_ptr;

    name        = controller->GetDeviceName();
    vendor      = "MSI";
    type        = DEVICE_TYPE_MOTHERBOARD;
    description = "MSI Mystic Light Device (185-byte)";
    version     = controller->GetFWVersion();
    location    = controller->GetDeviceLocation();
    serial      = controller->GetSerial();

    std::size_t numofZones = controller->GetNumofZones();
    const MSI_ZONE* supportedZones = controller->GetSupportedZones();

    for (std::size_t i = 0; i < numofZones; ++i)
    {
        for (std::size_t j = 0; j < NUMOF_ZONES; ++j)
        {
            if (led_zones[j].zoneType == supportedZones[i])
            {
                zoneDescription.push_back(&led_zones[j]);
                break;
            }
        }
    }

    SetupModes();
    SetupZones();
}

RGBController_MSIMysticLight185::~RGBController_MSIMysticLight185()
{
    delete controller;
}

void RGBController_MSIMysticLight185::SetupZones()
{
    /*-------------------------------------------------*\
    | Clear any existing color/LED configuration        |
    \*-------------------------------------------------*/
    leds.clear();
    colors.clear();

    bool firstRun = false;

    if (zones.size() == 0)
    {
        firstRun = true;
    }

    if (firstRun)
    {
        /*---------------------------------------------------------*\
        | Set up zones                                              |
        \*---------------------------------------------------------*/
        for(std::size_t zone_idx = 0; zone_idx < zoneDescription.size(); zone_idx++)
        {
            const ZoneDescription* zd      = zoneDescription[zone_idx];

            zone new_zone;

            new_zone.name           = zd->name;
            new_zone.type           = ZONE_TYPE_LINEAR;
            new_zone.leds_min       = zd->minLeds;

            int maxLeds = zd->maxLeds;
            if (zd->zoneType == MSI_ZONE_ON_BOARD_LED_0)
            {
                maxLeds = controller->GetMaxOnboardLeds();
            }

            new_zone.leds_max       = maxLeds;
            new_zone.leds_count     = maxLeds;
            new_zone.matrix_map     = NULL;
            zones.push_back(new_zone);
        }
    }


    /*---------------------------------------------------------*\
    | Set up LEDs                                               |
    \*---------------------------------------------------------*/
    for(std::size_t zone_idx = 0; zone_idx < zoneDescription.size(); zone_idx++)
    {
        controller->SetCycleCount(zoneDescription[zone_idx]->zoneType, zones[zone_idx].leds_count);

        for(std::size_t led_idx = 0; led_idx < zones[zone_idx].leds_count; led_idx++)
        {
            led new_led;
            
            new_led.name = zones[zone_idx].name + " LED ";
            if(zones[zone_idx].leds_count > 1)
            {
                new_led.name.append(std::to_string(led_idx + 1));
            }

            new_led.value = zoneDescription[zone_idx]->zoneType;

            leds.push_back(new_led);
        }
    }

    SetupColors();
}

void RGBController_MSIMysticLight185::ResizeZone(int zone, int new_size)
{
    if(static_cast<size_t>(zone) >= zones.size())
    {
        return;
    }

    if(((unsigned int)new_size >= zones[zone].leds_min) && ((unsigned int)new_size <= zones[zone].leds_max))
    {
        zones[zone].leds_count = new_size;
        SetupZones();
    }
}

void RGBController_MSIMysticLight185::SetCustomMode()
{
    active_mode = 0;
}

void RGBController_MSIMysticLight185::DeviceUpdateLEDs()
{
    for (std::size_t zone_idx = 0; zone_idx < zones.size(); zone_idx++)
    {
        for (int led_idx = zones[zone_idx].leds_count - 1; led_idx >= 0; led_idx--)
        {
            UpdateLed(static_cast<int>(zone_idx), led_idx);
        }
    }
    controller->Update((modes[active_mode].flags & MODE_FLAG_AUTOMATIC_SAVE) != 0);
}

void RGBController_MSIMysticLight185::UpdateZoneLEDs(int zone)
{
    for (int led_idx = zones[zone].leds_count - 1; led_idx >= 0; led_idx--)
    {
        UpdateLed(zone, led_idx);
    }
    controller->Update((modes[active_mode].flags & MODE_FLAG_AUTOMATIC_SAVE) != 0);
}

void RGBController_MSIMysticLight185::UpdateSingleLED(int led)
{
    int zone_index = IndexOfZoneForType(static_cast<MSI_ZONE>(leds[led].value));
    if (zone_index == -1) {
        std::cerr << "could not find zone for type " << leds[led].value << std::endl;
        return;
    }
    int led_index = led - zones[zone_index].start_idx;
    UpdateLed(zone_index, led_index);
    controller->Update((modes[active_mode].flags & MODE_FLAG_AUTOMATIC_SAVE) != 0);
}

void RGBController_MSIMysticLight185::DeviceUpdateMode()
{
    if (modes[active_mode].value == MSI_MODE_DIRECT_DUMMY)
    {
        controller->SetDirectMode(true);
    }
    else
    {
        controller->SetDirectMode(false);
        for (std::size_t zone_idx = 0; zone_idx < zones.size(); zone_idx++)
        {
            for (int led_idx = zones[zone_idx].leds_count - 1; led_idx >= 0; led_idx--)
            {
                UpdateLed(static_cast<int>(zone_idx), led_idx);
            }
        }
        controller->Update((modes[active_mode].flags & MODE_FLAG_AUTOMATIC_SAVE) != 0);
    }
}

void RGBController_MSIMysticLight185::DeviceSaveMode()
{
     controller->Update(true);
}

void RGBController_MSIMysticLight185::SetupModes()
{
    constexpr unsigned int PER_LED_ONLY = MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_MANUAL_SAVE;
    constexpr unsigned int RANDOM_ONLY  = MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_MANUAL_SAVE;
    constexpr unsigned int COMMON       = RANDOM_ONLY | MODE_FLAG_HAS_PER_LED_COLOR;

    SetupMode("Direct",                     MSI_MODE_DIRECT_DUMMY,                  MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR);
    SetupMode("Static",                     MSI_MODE_STATIC,                        MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_MANUAL_SAVE);
    // SetupMode("Off",                        MSI_MODE_DISABLE,                       0);
    SetupMode("Breathing",                  MSI_MODE_BREATHING,                     PER_LED_ONLY);
    SetupMode("Flashing",                   MSI_MODE_FLASHING,                      COMMON);
    SetupMode("Double flashing",            MSI_MODE_DOUBLE_FLASHING,               COMMON);
    SetupMode("Lightning",                  MSI_MODE_LIGHTNING,                     PER_LED_ONLY);
    // SetupMode("MSI Marquee",                MSI_MODE_MSI_MARQUEE,                   COMMON);
    SetupMode("Meteor",                     MSI_MODE_METEOR,                        COMMON);
    // SetupMode("Water drop",                 MSI_MODE_WATER_DROP,                    COMMON);
    // SetupMode("MSI Rainbow",                MSI_MODE_MSI_RAINBOW,                   RANDOM_ONLY);
    // SetupMode("Pop",                        MSI_MODE_POP,                           COMMON);
    // SetupMode("Rap",                        MSI_MODE_RAP,                           COMMON);
    // SetupMode("Jazz",                       MSI_MODE_JAZZ,                          COMMON);
    // SetupMode("Play",                       MSI_MODE_PLAY,                          COMMON);
    // SetupMode("Movie",                      MSI_MODE_MOVIE,                         COMMON);
    SetupMode("Color ring",                 MSI_MODE_COLOR_RING,                    RANDOM_ONLY);
    SetupMode("Planetary",                  MSI_MODE_PLANETARY,                     RANDOM_ONLY);
    SetupMode("Double meteor",              MSI_MODE_DOUBLE_METEOR,                 RANDOM_ONLY);
    SetupMode("Energy",                     MSI_MODE_ENERGY,                        RANDOM_ONLY);
    SetupMode("Blink",                      MSI_MODE_BLINK,                         COMMON);
    SetupMode("Clock",                      MSI_MODE_CLOCK,                         RANDOM_ONLY);
    SetupMode("Color pulse",                MSI_MODE_COLOR_PULSE,                   COMMON);
    SetupMode("Color shift",                MSI_MODE_COLOR_SHIFT,                   RANDOM_ONLY);
    SetupMode("Color wave",                 MSI_MODE_COLOR_WAVE,                    COMMON);
    SetupMode("Marquee",                    MSI_MODE_MARQUEE,                       PER_LED_ONLY);
    // SetupMode("Rainbow",                    MSI_MODE_RAINBOW,                       COMMON);
    SetupMode("Rainbow wave",               MSI_MODE_RAINBOW_WAVE,                  RANDOM_ONLY);
    SetupMode("Visor",                      MSI_MODE_VISOR,                         COMMON);
    // SetupMode("JRainbow",                   MSI_MODE_JRAINBOW,                      COMMON);
    SetupMode("Rainbow flashing",           MSI_MODE_RAINBOW_FLASHING,              RANDOM_ONLY);
    // SetupMode("Rainbow double flashing",    MSI_MODE_RAINBOW_DOUBLE_FLASHING,       COMMON);
    // SetupMode("Random",                     MSI_MODE_RANDOM,                        COMMON);
    // SetupMode("Fan control",                MSI_MODE_FAN_CONTROL,                   COMMON);
    // SetupMode("Off 2",                      MSI_MODE_DISABLE_2,                     COMMON);
    // SetupMode("Color ring flashing",        MSI_MODE_COLOR_RING_FLASHING,           COMMON);
    SetupMode("Color ring double flashing", MSI_MODE_COLOR_RING_DOUBLE_FLASHING,    RANDOM_ONLY);
    SetupMode("Stack",                      MSI_MODE_STACK,                         COMMON);
    // SetupMode("Corsair Que",                MSI_MODE_CORSAIR_QUE,                   COMMON);
    // SetupMode("Fire",                       MSI_MODE_FIRE,                          COMMON);
    // SetupMode("Lava",                       MSI_MODE_LAVA,                          COMMON);
}

void RGBController_MSIMysticLight185::UpdateLed(int zone, int led)
{
    unsigned char red   = RGBGetRValue(zones[zone].colors[led]);
    unsigned char grn   = RGBGetGValue(zones[zone].colors[led]);
    unsigned char blu   = RGBGetBValue(zones[zone].colors[led]);

    if (controller->isDirectMode())
    {
        controller->SetLedColor((MSI_ZONE)zones[zone].leds[led].value, led, red, grn, blu);
    }
    else
    {
        if (led == 0)
        {
            bool random         = modes[active_mode].color_mode == MODE_COLORS_RANDOM;
            MSI_MODE      mode  = (MSI_MODE)(modes[active_mode].value);
            MSI_SPEED     speed = (MSI_SPEED)(modes[active_mode].speed);

            controller->SetMode((MSI_ZONE)zones[zone].leds[led].value, mode, speed, MSI_BRIGHTNESS_LEVEL_100, random);
            controller->SetZoneColor((MSI_ZONE)zones[zone].leds[led].value, red, grn, blu, red, grn, blu);
        }
    }
}

void RGBController_MSIMysticLight185::SetupMode(const char *name, MSI_MODE mod, unsigned int flags)
{
    mode Mode;
    Mode.name       = name;
    Mode.value      = mod;
    Mode.flags      = flags;

    if (flags & MODE_FLAG_HAS_PER_LED_COLOR)
    {
        Mode.color_mode = MODE_COLORS_PER_LED;
    }
    else
    {
        Mode.color_mode = MODE_COLORS_RANDOM;
    }

    if (flags & MODE_FLAG_HAS_SPEED)
    {
        Mode.speed      = MSI_SPEED_MEDIUM;
        Mode.speed_max  = MSI_SPEED_HIGH;
        Mode.speed_min  = MSI_SPEED_LOW;
    }
    else
    {
        /*---------------------------------------------------------*\
        | For modes without speed this needs to be set to avoid     |
        | bad values in the saved profile which in turn corrupts    |
        | the brightness calculation when loading the profile       |
        \*---------------------------------------------------------*/
        Mode.speed      = 0;
        Mode.speed_max  = 0;
        Mode.speed_min  = 0;
    }

    modes.push_back(Mode);
}
